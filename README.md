# URL Shortener Service #
A simple service for providing short URLs for a given full URL. 
POST a long URL as body data to "/submit" endpoint and get back a short URL.
GET a short URL as returned above to be redirected to the full URL originally provided. 
Backed by Redis.

## Prerequisites ##
* `docker` CLI and daemon installed locally
* `docker-compose` installed locally
* All network configs happy
    * Proxies and firewalls sane for use
    * Can access outside internet (for pulling images and deps)

## Config Options ##
* Env vars (see `Dockerfile`)
    * `SHORTENER_BASE_URL` | Base URL to use for shortening
        * Ex: in `localhost:8080` the base URL is `localhost`
    * `SHORTENER_PORT` | Port to bind to
        * Ex: in `localhost:8080` the port is `8080`
    * `SHORTENER_LOG_LEVEL` | Level to use for logging 
        * Set to DEBUG to see extra details

## Up and Running ##
1. Clone this repository

2. Run `docker-compose` commands 
    * `cd url-shortener; docker-compose build; docker-compose up`

3. Trying posting a short URL
    * `curl -s -XPOST localhost:8080/submit -d "https://justinflowers.ca/"`
    * Will return something like `localhost:8080/1a53372`
    
4. Go to URL returned above in your browser of choice
    * Alternatively can test with `curl -v $RETURNED_URL`

## Test Plan ##
* Post an invalid URL
    * `curl -v -XPOST localhost:8080/submit -d "https://justinflowers/"`
    * Should return a 400
* Try to inject something dangerous at "submit()" endpoint
    *  `curl -s -XPOST localhost:8080/submit -d 'https://justinflowers.ca/\" + run_this_arbitrarily()'`
    * Should work without allowing input data to affect normal flow
    * Full link should look like: 'https://justinflowers.ca/%5C%22%20%2B%20run_this_arbitrarily%28%29'
* Get an invalid or non-existent short link
    * In your browser try something like `localhost:8080/1` (too short; should be 7 chars at least)
    * Can also try `curl -v http://localhost:8080/1`
    * Should return a 404
* Try to inject something dangerous retrieving short link
    * Try something like `curl -v 'http://localhost:8080/run_this_abitrarily()'`
    * Should return a 400
* Try POSTing a more complicated full URL with special characters
    * `curl -s -XPOST localhost:8080/submit -d 'https://www.youtube.com/watch?v=5qap5aO4i9A'`
    * Should create short URL happily and redirect to correct full URL happily
    
## Considerations for the Future ##
* Will SSL termination occur in code or in layer above?
    * How does that affect design of code and container?
* This packages "validator_collection" just for a one-liner to validate URL data
    * Is a whole new lib worth it here? Maybe could get away with well tested regex
* Is there anything we're missing to support K8s and cloud deploys?
* How can the redis error handling be made more robust?
* How should the redis cache be persisted? 
    * Will we host it ourselves?
        * Use block storage? Or other distributed storage infrastructure?
    * Will we use an external service?
        * Do they have acceptable speed, DR, and data tolerance?
* Is it a problem if multiple keys have the same value?
    * How do we handle cleaning up old keys if necessary? Time based?

## Why Redis? ##
* Considering this app only needs simple key/value retrieval, don't see the point in a larger more full featured DB
    * Redis is lighter weight
    * Redis is best designed for key/value retrieval
    * Redis should scale better
* Would consider different data stores if functionality needed to be more complex
    * Would also consider if one wants to avoid having two (or more) unique keys for the same value