# Author: Justin Flowers
# Email: justin@justinflowers.ca
# Copyright: (c) 2020 by Justin Flowers
# License: Apache License 2.0
import time
import uuid
import redis
import os
from flask import Flask, redirect, request
import logging
from urllib.parse import quote
from validator_collection import checkers
# Yes, yes... I know I'm packaging a new lib (validator_collection) just for a one
# liner (see "submit()" function); in a true scenario I would work with senior devs
# to decide if if I should write my own regex instead

# Should have a dynamic base url & port to support container based deploys
baseurl = os.getenv('SHORTENER_BASE_URL', "localhost")
port = os.getenv('SHORTENER_PORT', "8080")
loglevel = os.getenv('SHORTENER_LOG_LEVEL', logging.INFO)

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)
app.logger.setLevel(loglevel)

def create_short_link(link):
    """Takes in a full URL link, returns the short hash generated for link."""
    retries = 5
    # I pick 7 for max length as it matches "2WCgpid"
    short = str(uuid.uuid4())[:7]
    # Cute little retry mechanism from Docker Compose Flask examples
    # Would make this more robust if I had more time
    while True:
        try:
            # Regenerate short if already in cache
            while cache.exists(short):
                short = str(uuid.uuid4())[:7]
            cache.set(short, link)
            return short
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


def get_full_link(short):
    """Takes in a hash short, returns the full URL cached for short."""
    retries = 5
    # Cute little retry mechanism from Docker Compose Flask examples
    # Would make this more robust if I had more time
    while True:
        try:
            full = cache.get(short)
            if full is not None:
                return full.decode("utf-8")
            return None
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/submit', methods=['POST'])
def submit():
    """Takes in message data for a URL; sanitizes and validates content then returns
    a shortened URL back as response data. If data fails sanitization and validation
    then returns a 400.
    """
    data = request.get_data()
    sanitized = quote(data, safe='/:?&=')
    if not checkers.is_url(sanitized):
        return '400 Bad Request: URL provided is malformed.', 400
    short = create_short_link(sanitized)
    app.logger.debug("Created short hash {} for full link {}".format(short, sanitized))
    return '{}:{}/{}'.format(baseurl, port, short)


@app.route("/<short>", methods=['GET'])
def redirect_to_full_link(short):
    """Takes in hash short from route and if found redirects to full URL; otherwise returns a 404."""
    sanitized = str(short)
    if not sanitized.isalnum():
        return '400 Bad Request: Malformed short URL.', 400
    full = get_full_link(sanitized)
    if full is None:
        return '404 Not Found: Cannot find full URL for given short URL.', 404
    app.logger.debug("Redirecting to full link {} for short hash {}".format(full, sanitized))
    return redirect(full)
