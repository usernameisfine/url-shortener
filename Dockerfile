# A good lightweight Alpine Python container (should scale well with load)
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=main.py
ENV FLASK_RUN_HOST=0.0.0.0
# Change these vars to reflect new hostname and port here and in "main.py"
ENV SHORTENER_BASE_URL=localhost
ENV SHORTENER_PORT=8080
# Change to DEBUG to see logging details
ENV SHORTENER_LOG_LEVEL=INFO
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8080
COPY . .
CMD ["sh", "-c", "flask run -p $SHORTENER_PORT"]
